FROM python:3.6

COPY . /source
WORKDIR /source

RUN python -m pip install -U pip setuptools
RUN python -m pip install -r requirements.txt


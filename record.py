# -*- coding: utf-8 -*-

"""Stream recorder for wen radio streams."""
import argparse
import sys
import urllib
import uuid
from io import BufferedReader, BytesIO
from pathlib import Path
from typing import Tuple

import pendulum
import requests
import sentry_sdk
from logbook import FileHandler, Logger, NestedSetup, NullHandler, StreamHandler  # NOQA

sentry_sdk.init('https://a25ced3957d949d1a58d5cafd8b48149@sentry.io/2408845', environment='new_version')

ORIGINAL_HTTP_CLIENT_READ_STATUS = urllib.request.http.client.HTTPResponse._read_status  # noqa: WPS437,WPS219

logger = Logger('record.py')


def get_logger(stream_handler_log_level: str = 'ERROR') -> Tuple[NestedSetup, Logger]:
    """Configure the logger."""
    format_string = (
        '{record.time:%Y-%m-%d %H:%M:%S} {record.level_name}: '
        '{record.filename}:{record.lineno}:{record.func_name} {record.message}'
    )

    setup = NestedSetup([
        NullHandler(),
        FileHandler(
            'record_stream.log',
            level='INFO',
            format_string=format_string,
        ),
        StreamHandler(
            stream=sys.stdout,
            level=stream_handler_log_level,
            format_string=format_string,
            bubble=True,
        ),
    ])
    return setup, Logger('record_stream')


class InterceptedHTTPResponse(object):
    """Empty class as placeholder for the HttpResponse."""


def nice_to_ICY(self):  # noqa: N802
    """A kind of mock patch to allow an ICY response to be valid."""
    line = self.fp.readline().replace(b'ICY 200 OK\r\n', b'HTTP/1.0 200 OK\r\n')
    intercepted_self = InterceptedHTTPResponse()
    intercepted_self.fp = BufferedReader(BytesIO(line))
    intercepted_self.debuglevel = self.debuglevel
    intercepted_self._close_conn = self._close_conn  # noqa:WPS437
    return ORIGINAL_HTTP_CLIENT_READ_STATUS(intercepted_self)


def get_date_and_time_prefix() -> str:
    """Get a prefix for the record files containing current date and time."""
    date_time_prefix = pendulum.now(tz='Europe/Berlin').strftime('%Y%m%d_%H%M%S_')
    logger.debug(f'prefix {date_time_prefix}')
    return date_time_prefix


def get_output_file_path(output_folder_path: Path, broadcast_name: str) -> Path:
    """Get the default path ro a given broadcast recording."""
    output_file_path = output_folder_path / '{date_string}{broadcast_name}.mp3'.format(
        date_string=get_date_and_time_prefix(),
        broadcast_name=broadcast_name,
    )
    logger.debug(f'[default] output file path {output_file_path!s}')
    return output_file_path


def get_output_file_handle(output_folder_path: Path, broadcast_name: str):
    """Get the handle for a broadcast recording.

    Ensures that we always get back some handle to write to.
    """
    logger.info('Start evaluation of output file path')
    write_binary_mode = 'wb'  # write mode for open()
    try:
        logger.debug('Try default file path')
        return open(
            str(get_output_file_path(output_folder_path, broadcast_name)),
            write_binary_mode,
        )
    except Exception as error_level1:
        try:
            logger.info('Default path failed. Going ahead with unprefixed path in current directory')
            logger.error(error_level1)
            return open(
                '{broadcast_name}.mp3'.format(broadcast_name=broadcast_name),
                write_binary_mode,
            )
        except Exception as error_level2:
            logger.error(error_level2)
            random_file_name = '{file_name}.mp3'.format(file_name=str(uuid.uuid1()))
            logger.info(f'That failed too. Take random file name {random_file_name}')
            return open(
                random_file_name,
                write_binary_mode,
            )


def get_proxies_kwarg(proxy: str):
    """Get a proxy dictionary for requests."""
    if proxy:
        logger.debug(f'configure proxy {proxy}')
        return {
            'http': proxy,
            'https': proxy,
            'ftp': proxy,
        }


def record_stream(
        proxy: str, *,
        stream_url: str,
        duration_in_minutes: int,
        output_folder_path: Path,
        broadcast_name: str,
) -> bool:
    """Main recording function."""
    logger.debug('Start record')
    extra_recording_time = 2  # minutes to extend recording to be sure all is recorded
    end_time = pendulum.now().add(minutes=duration_in_minutes + extra_recording_time)

    # mock patch to allow ICY responses
    urllib.request.http.client.HTTPResponse._read_status = nice_to_ICY  # noqa:WPS 219

    stream_response = requests.get(stream_url, stream=True, proxies=get_proxies_kwarg(proxy))
    logger.debug(f'got stream response for {stream_url}')
    output_file_handle = get_output_file_handle(
        output_folder_path=output_folder_path,
        broadcast_name=broadcast_name,
    )
    logger.debug('got output file handle')
    try:
        for block in stream_response.iter_content(1024):
            output_file_handle.write(block)
            if pendulum.now() > end_time:
                logger.debug(f'{pendulum.now()} > {end_time} {pendulum.now() > end_time}')  # noqa: WPS221
                break
    except Exception as error:
        logger.exception(error)
    finally:
        logger.info(f'Recording to {output_file_handle.name} has been finished.')
        output_file_handle.close()
    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--debug',
        dest='debug',
        action='store_true',
    )
    parser.add_argument(
        '--stream-url',
        dest='stream_url',
        type=str,
        required=True,
    )
    parser.add_argument(
        '--duration',
        dest='duration',
        type=int,
        required=True,
    )
    parser.add_argument(
        '--output-folder',
        dest='output_folder',
        type=Path,
        required=True,
    )
    parser.add_argument(
        '--broadcast-name',
        dest='broadcast_name',
        required=True,
    )

    parser.add_argument(
        '--proxy',
        dest='proxy',
    )

    args = parser.parse_args()
    setup, logger = get_logger(stream_handler_log_level='DEBUG' if args.debug else 'ERROR')
    with setup.applicationbound():
        record_stream(
            stream_url=args.stream_url,
            duration_in_minutes=args.duration,
            output_folder_path=Path(args.output_folder),
            broadcast_name=args.broadcast_name,
            proxy=args.proxy,
        )
